public class Move {
    private Square from;
    private Square to;

    public Square getFrom() {
        // Return the source square of the move
        throw new UnsupportedOperationException();
    }

    public Square getTo() {
        // Return the destination square of the move
        throw new UnsupportedOperationException();
    }
}
